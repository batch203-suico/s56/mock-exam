To start using the built-in tests, run <code>npm install</code> first then issue the <code>npm test</code> at the root directory of this project.

WARNING: Do not change any code inside <code>test.js</code>.

_Take a screenshot of your terminal when you’re done, attached an image in the mock tech folder._ **(No need to send in Hangouts)**

**Create a new repository named s56 and push your mock-tech exam.**

Add the link in Boodle: _WDC028v1.5b-56 | Mock Technical Exam (Concepts and Theory + Function Coding)_
